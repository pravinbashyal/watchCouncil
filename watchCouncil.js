var cheerio = require('cheerio');

var watch = function() {
	var $;
	var checkText;
	return {
		loadText: function(text) {
			$ = cheerio.load(text);
		},
		contentChanged: function(selector, callback) {
			var watchText = $(selector).text() || '';
			var arr = [];
			arr.push(watchText);
			console.log(arr);
			if(watchText === checkText) {
				return callback(null, false);
			} else {
				return callback(null, watchText);
			}
			// return watchText === checkText;
		},
		setDefault: function(text) {
			checkText = text;
		}
	};
}();

module.exports = watch;
