'use strict';

var mailer = require('./mailer');
var watcher = require('./watchCouncil');
var request = require('request');
var cron = require('cron');

var watch = function() {

	request({uri: 'https://ielts.britishcouncil.org/nepal'}, function(err, data) {

		watcher.setDefault('\r\n\tThere currently are no test dates available\r\n\r\n');
		// watcher.setDefault('test');
		watcher.loadText(data.body);
		// console.log(data.body);
		watcher.contentChanged('#ctl00_ContentPlaceHolder1_ddlDateMonthYear', function(err, changed) {
			if(changed) {
				console.log('changed');
				mailer(changed);
				cronJob.stop();
			} else {
				console.log('unchanged');
			}
		});

	});

};

var cronJob = cron.job('*/10 * * * * *', function() {
	watch();
	console.info('cron job completed');
});

cronJob.start();

var express = require('express');
var app = express();

app.set('port', process.env.PORT || 3000);

app.get('/', function(req, res) {
	res.send('Watch Council');
});

var server = app.listen(app.get('port'), function() {
  var host = server.address().address;
  var port = server.address().port;

  console.log('Example app listening at http://%s:%s', host, port);
});
